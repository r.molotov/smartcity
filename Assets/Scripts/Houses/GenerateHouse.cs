﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateHouse : MonoBehaviour
{

    [Header("Models")]

    [Tooltip("There are 3 house types with adding parts in order: wall, window, roof, sideRoof, angleRoof")]
    [TextArea]
    public string READ = "!!! There are 3 house types with adding parts in order: wall, window, roof, sideRoof, angleRoof(IF ther are not sideRoof and AngleRoof use usual Roof)";

    [Tooltip("There are 3 house types with adding parts in order: wall, window, roof, sideRoof, angleRoof")]
    public GameObject[] SimpleHouse;


    public Material[] SimpleHouseMaterials;

    [Tooltip("There are 3 house types with adding parts in order: wall, window, roof, sideRoof, angleRoof")]
    public GameObject[] MiddleHouse;

    [Range(0, 2)]
    public int houseType;

    [Range(0,2)]
    public int Wall_tex;
    [Range(0,2)]
    public int Roof_tex;

    public int length;
    public int width;
    public int height;

    

    public Vector3 Coords;


    // Start is called before the first frame update
    void Start()
    {
        
       
    }


    private List<int[]> repeats = new List<int[]>() {
            new int[] {1, 1},
            new int[] {1, 3}
        };
    public int Repeats(int length, int heightNow)
    {
        if (heightNow == 0 || heightNow == height)
        {
            return 0;
        }
        else
        {
            if (length % 2 == 0)
                return 1;
            else
                return 1;
        }
    }
    public void Generate()
    {
        Coords = new Vector3(-length + 1.5f + gameObject.transform.position.x, gameObject.transform.position.y, width + 0.5f + gameObject.transform.position.z);
        #region SettingsObjects
        List<GameObject[]> modelsHouse = new List<GameObject[]>() { SimpleHouse, MiddleHouse};

        GameObject wallInitiate =  modelsHouse[houseType][0];
        wallInitiate.GetComponent<MeshRenderer>().sharedMaterial = SimpleHouseMaterials[8 + Wall_tex];

        GameObject windowInitiate = modelsHouse[houseType][1];

        GameObject roofInitiate = modelsHouse[houseType][2];
        roofInitiate.GetComponent<MeshRenderer>().sharedMaterial = SimpleHouseMaterials[11 + Roof_tex];

        GameObject sideRoofInitiate = modelsHouse[houseType][3];
        sideRoofInitiate.GetComponent<MeshRenderer>().sharedMaterial = SimpleHouseMaterials[11 + Roof_tex];

        GameObject angleRoofInitiate = modelsHouse[houseType][4];
        angleRoofInitiate.GetComponent<MeshRenderer>().sharedMaterial = SimpleHouseMaterials[11 + Roof_tex];

        #endregion

        List<GameObject> models = new List<GameObject>() {wallInitiate, windowInitiate, roofInitiate, sideRoofInitiate, angleRoofInitiate};


        #region HouseWalls

        for (int z = 0; z < height; z++)
        {
            for (int x = 0; x < length; x++)
            {
                Material[] matArray = windowInitiate.GetComponent<MeshRenderer>().sharedMaterials;
                matArray[0] = SimpleHouseMaterials[8 + Wall_tex];
                matArray[1] = SimpleHouseMaterials[Random.Range(0, 7)];
                windowInitiate.GetComponent<MeshRenderer>().sharedMaterials = matArray;

                Instantiate(models[Repeats(x, z)], new Vector3(Coords.x + x * 2, Coords.y + z * 2, Coords.z), Quaternion.identity,transform);
                Instantiate(models[Repeats(x, z)], new Vector3(Coords.x + x * 2, Coords.y + z * 2, Coords.z - 2 * width), Quaternion.AngleAxis(180, Vector3.up), transform);
            }
        }

        for (int z = 0; z < height; z++)
        {
            for (int x = 0; x < width; x++)
            {
                Material[] matArray = windowInitiate.GetComponent<MeshRenderer>().sharedMaterials;
                matArray[0] = SimpleHouseMaterials[8 + Wall_tex];
                matArray[1] = SimpleHouseMaterials[Random.Range(0, 7)];
                windowInitiate.GetComponent<MeshRenderer>().sharedMaterials = matArray;

                Instantiate(models[Repeats(x, z)], new Vector3(Coords.x - 1, Coords.y + z * 2, Coords.z - x * 2 - 1), Quaternion.AngleAxis(-90, Vector3.up), transform);
                Instantiate(models[Repeats(x, z)], new Vector3(Coords.x - 1 + 2 * length, Coords.y + z * 2, Coords.z - x * 2 - 1), Quaternion.AngleAxis(90, Vector3.up), transform);

            }
        }

        #endregion


        #region Roof

        //center
        for (int y = 0; y < width - 1; y++)
        {
            for (int x = 0; x < length - 1; x++)
            {
                Instantiate(models[2], new Vector3(Coords.x + x * 2 + 1, Coords.y + height * 2 , Coords.z - 2 - y * 2), Quaternion.identity, transform);
            }
        }

        //Left Right Side
        for (int x = 0; x < length - 1; x++)
        {
            Instantiate(models[3], new Vector3(Coords.x + x * 2 + 1, Coords.y + height * 2, Coords.z), Quaternion.identity, transform);
            Instantiate(models[3], new Vector3(Coords.x + x * 2 + 1, Coords.y + height * 2, Coords.z - width * 2), Quaternion.AngleAxis(180, Vector3.up), transform);
        }

        //Toward Back
        for (int z = 0; z < width + 1; z++)
        {
            Quaternion rot = Quaternion.identity;
            GameObject initiateModel = models[3];
            if (z == 0 || z == width)
            {
                initiateModel = models[4];
                if (z != 0)
                    rot = Quaternion.AngleAxis(-90, Vector3.up);
            }
            else
            {
                initiateModel = models[3];
                rot = Quaternion.AngleAxis(-90, Vector3.up);
            }

            Instantiate(initiateModel, new Vector3(Coords.x - 1, Coords.y + height * 2, Coords.z - z * 2), rot, transform);
            Instantiate(initiateModel, new Vector3(Coords.x - 1 + length * 2, Coords.y + height * 2, Coords.z - z * 2), rot, transform);
        }

        #endregion

        transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //GetComponent<BoxCollider>().center = new Vector3(length, 0, -width);
        GetComponent<BoxCollider>().size = new Vector3(length*2, height*4, width*2);
        GetComponent<BoxCollider>().enabled = true;
        //foreach (Transform item in transform)
        //{
        //    item.gameObject.isStatic = true;
        //}
        StaticBatchingUtility.Combine(this.gameObject);
        GetComponent<PointInfo>().spawnpoint = transform.GetChild(0).gameObject;
        gameObject.tag = "IPoint";
        GetComponent<PointInfo>().MyID = 1;
        GetComponent<PointInfo>().Liveble = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
