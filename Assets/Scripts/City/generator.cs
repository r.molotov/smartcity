﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Drawing;
using Voronoi2;

public class generator : MonoBehaviour
{
    [SerializeField] Image Im;
    public Texture2D image;

	public System.Random seeder;
	public	Voronoi voroObject;
    public GameObject road;
    public GameObject house;
    
    public int width;
    public int height;
	public int siteCount = 20;
    List<GameObject> Roads= new List<GameObject>(0) ;
    int[] LivePlase;
    [SerializeField] CityManager Man;
    [SerializeField] int AllBuilds;
    [SerializeField] int MaxBuildX;
    [SerializeField] int MaxBuildY;

    public Terrain terrain;
    float[,] terrain_heights;
    public float terrain_generation_scale = 60;
    public int terrain_generation_octaves = 1;
    public float terrain_generation_lacunarity = 4.5f;
    public float terrain_generation_persistance = 0.2f;
    public float terrain_generation_power = 0.15f;

    public void DrawCircle(Texture2D tex, UnityEngine.Color color, int x, int y, int radius = 3)
    {
        float rSquared = radius * radius;

        for (int u = Mathf.Max(0, x - radius); u < Mathf.Min(tex.width, x + radius + 1); u++)
            for (int v = Mathf.Max(0, y - radius); v < Mathf.Min(tex.height, y + radius + 1); v++)
                //if ((x - u) * (x - u) + (y - v) * (y - v) < rSquared)
                tex.SetPixel(u, v, color);

        
    }

    void DrawLine(Texture2D tex, int x0, int y0, int x1, int y1, int width, UnityEngine.Color col)
    {
        int dy = (int)(y1-y0);
        int dx = (int)(x1-x0);
        int stepx, stepy;
    
        if (dy < 0) {dy = -dy; stepy = -1;}
        else {stepy = 1;}
        if (dx < 0) {dx = -dx; stepx = -1;}
        else {stepx = 1;}
        dy <<= 1;
        dx <<= 1;
    
        float fraction = 0;
    
        DrawCircle(tex, col, x0, y0, width);
        if (dx > dy) {
            fraction = dy - (dx >> 1);
            while (Mathf.Abs(x0 - x1) > 1) {
                if (fraction >= 0) {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                DrawCircle(tex, col, x0, y0, width);
                //tex.SetPixel(x0, y0, col);
            }
        }
        else {
            fraction = dx - (dy >> 1);
            while (Mathf.Abs(y0 - y1) > 1) {
                if (fraction >= 0) {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                DrawCircle(tex, col, x0, y0, width);
                //tex.SetPixel(x0, y0, col);
            }
        }
    }

    void spreadPoints()
    {
        image = new Texture2D(width, height);
        List<PointF> sites = new List<PointF>();
        int seed = seeder.Next();
        System.Random rand = new System.Random(seed);

        for (int i = 0; i < siteCount; i++)
        {
            sites.Add(new PointF((float)(rand.NextDouble() * width), (float)(rand.NextDouble() * height)));
        }

        //for (int i = 0; i < sites.Count; i++)
        //{   
        //    DrawCircle(image, new UnityEngine.Color(1, 0, 0), (int)(Mathf.Round(sites[i].X-1.5f)), (int)(Mathf.Round(sites[i].Y-1.5f)), 2);
        //}

        List<GraphEdge> ge;
        ge = MakeVoronoiGraph(sites, image.width, image.height);

        for (int i = 0; i < ge.Count; i++)
        {
            DrawLine(image, (int)ge[i].x1, (int)ge[i].y1, (int)ge[i].x2, (int)ge[i].y2, 1, new UnityEngine.Color(1, 0, 0));

        }
        image.Apply();
        float[,] perlin = getnoise.GetSimplePerlin((float)rand.NextDouble() * 9999, (float)rand.NextDouble() * 9999, width, height, (width * height) / 2000, width / 2, 1f, 1f);
        float[,] mask = getnoise.CreateMask(width, height, width / 2.2f, 0.9f);
        int step = MaxBuildX + 1;
        for (int i = 0; i < width; i += 1)
        {
            for (int g = 0; g < height; g += 1)
            {
                if (mask[i, g] == 0f)
                {
                    perlin[i, g] = 0;
                    image.SetPixel(i, g, new UnityEngine.Color(0, 0, 0));
                }
            }
        }
        for (int i = 0; i < width - step + 1; i += step)
        {
            for (int g = 0; g < height - step + 1; g += step)
            {
                float val = 0;
                for (int x = 0; x < step; x++)
                {
                    for (int y = 0; y < step; y++)
                    {
                        val += perlin[i + x, g + y];
                        if (image.GetPixel(i + x, g + y) == new UnityEngine.Color(1, 0, 0) || !check_height_drop(g + x, i + y))
                        {
                            val = -999999f;
                        }
                    }
                }
                val /= (step * step);
                for (int x = 0; x < step; x++)
                {
                    for (int y = 0; y < step; y++)
                    {
                        perlin[i + x, g + y] = val > 0.45 ? val : 0;
                    }
                }
                if (val > 0.45)
                {
                    int a = Random.Range(1, AllBuilds);

                    GameObject bf = GameObject.Instantiate(house, transform.parent);
                    //теперь тут меньше кода. Вся стройка отдельно. Это очень неплохо помогает.
                    bf.transform.position = new Vector3(i + 1.5f, terrain.terrainData.GetInterpolatedHeight((float)(i + 0.5f) / (float)(width + 1), (float)(g + 0.5f) / (float)(height + 1)) + 0.1f, g + 1.5f);
                    //bf.GetComponent<GenerateHouse>().Generate();
                    bf.AddComponent<BuildConfig>();
                    bf.GetComponent<BuildConfig>().AddType(a);
                    bf.GetComponent<BuildConfig>().AddManePref(house);
                    //bf.GetComponent<BuildConfig>().Build();

                    Roads.Add(bf);
                    // bf.transform.localScale = new Vector3(5, val * 15, 5);





                    //GameObject bf = GameObject.Instantiate(house, transform.parent);
                    //bf.transform.position = new Vector3(i + 1.5f, 0, g + 1.5f);
                    //bf.transform.localScale = new Vector3(5, val * 15, 5);

                }
            }
        }
        for (int i = 0; i < width; i++)
        {
            for (int g = 0; g < height; g++)
            {
                if (image.GetPixel(i, g) != new UnityEngine.Color(1, 0, 0))
                {
                    image.SetPixel(i, g, new UnityEngine.Color(perlin[i, g], perlin[i, g], perlin[i, g]));
                    //GameObject bf = GameObject.Instantiate(grass, transform.parent.parent);

                    //bf.transform.position = new Vector3(i + 0.5f, 0.1f, g + 0.5f);

                }
                else if (mask[i, g] != 0f && check_height_drop(g, i))
                {
                    //GameObject bf = GameObject.Instantiate(grass, transform.parent.parent);

                    //bf.transform.position = new Vector3(i + 0.5f, 0.1f, g + 0.5f);
                    GameObject bf = GameObject.Instantiate(house, new Vector3(i + 0.5f, terrain.terrainData.GetInterpolatedHeight((float)(i + 0.5f) / (float)(width + 1), (float)(g + 0.5f) / (float)(height + 1)), g + 0.5f), Quaternion.identity, transform.parent);
                    bf.AddComponent<BuildConfig>();
                    bf.GetComponent<BuildConfig>().AddType(0);
                    bf.GetComponent<BuildConfig>().AddManePref(house);
                    Roads.Add(bf);

                    //bf.transform.position = new Vector3(i + 0.5f, terrain.terrainData.GetInterpolatedHeight((float)(i + 0.5f) / (float)(width + 1), (float)(g + 0.5f) / (float)(height + 1)), g + 0.5f);
                }
                //else {
                //    GameObject bf = GameObject.Instantiate(grass, transform.parent.parent);
                //    bf.transform.position = new Vector3(i + 0.5f, 0.1f, g + 0.5f);
                //}

            }
        }
        image.Apply();
        float[,,] splatmapData = new float[terrain.terrainData.alphamapWidth, terrain.terrainData.alphamapHeight, terrain.terrainData.alphamapLayers]; // создание карты текстур (ориг. название не преводимо онлайн переводчиками) 
        for (int x = 0; x < terrain.terrainData.alphamapWidth; x++)
        {
            for (int y = 0; y < terrain.terrainData.alphamapHeight; y++)
            {
                if (mask[y, x] != 0f && image.GetPixel(y, x) == new UnityEngine.Color(1, 0, 0))
                {
                    splatmapData[x, y, 0] = 0;
                    splatmapData[x, y, 1] = 1;
                }
                else
                {
                    splatmapData[x, y, 0] = 1;
                    splatmapData[x, y, 1] = 0;
                }
            }
        }
        terrain.terrainData.SetAlphamaps(0, 0, splatmapData); // применение карты
        terrain.Flush();
    }

    private List<GraphEdge> MakeVoronoiGraph(List<PointF> sites, int width, int height)
    {
        double[] xVal = new double[sites.Count];
        double[] yVal = new double[sites.Count];
        for (int i = 0; i < sites.Count; i++)
        {
            xVal[i] = sites[i].X;
            yVal[i] = sites[i].Y;
        }
        return voroObject.generateVoronoi(xVal, yVal, 0, width, 0, height);

    }
    bool check_height_drop(int x, int y){
        return terrain_heights[x, y] == terrain_heights[x + 1, y + 1];
    }
    void generateTerrain(){
        terrain_heights = getnoise.GetMaskedMultiNoise(width + 1, height + 1, terrain_generation_scale, terrain_generation_octaves, terrain_generation_persistance, terrain_generation_lacunarity, terrain_generation_power);
        float v = 0;
        for(int i = 0; i < width + 1;i+=2){
            for(int g =0; g < height + 1; g+=2){
                
                float sm = 0;
                for(int x = 0; x < 2;x++){
                    for(int y = 0; y < 2;y++){
                        sm += terrain_heights[Mathf.Min(width, i + x), Mathf.Min(height, g + y)];
                    }
                }
                sm /= 4f;
                if(i == width){
                    sm = terrain_heights[i - 1, g];
                    for(int x = 0; x < 2;x++){
                        for(int y = 0; y < 2;y++){
                            terrain_heights[Mathf.Min(width, i + x), Mathf.Min(height, g + y)] = sm;
                        }
                    }
                }
                else if(g == height){
                    sm = terrain_heights[i, g - 1];
                    for(int x = 0; x < 2;x++){
                        for(int y = 0; y < 2;y++){
                            terrain_heights[Mathf.Min(width, i + x), Mathf.Min(height, g + y)] = sm;
                        }
                    }
                }else{
                    for(int x = 0; x < 2;x++){
                        for(int y = 0; y < 2;y++){
                            terrain_heights[Mathf.Min(width, i + x), Mathf.Min(height, g + y)] = (int)(sm *100) / 2 / 250f;
                        }
                    }
                }
                v = Mathf.Max(v, terrain_heights[i, g]);
            }
        }

        terrain.terrainData.heightmapResolution = (int)width;
		terrain.terrainData.size = new Vector3 (width, height, height ); 
        terrain.terrainData.alphamapResolution = width;
        terrain.terrainData.SetHeights(0, 0, terrain_heights);
        terrain.Flush();




    }
    void Awake()
    {
        // !генерация тееррейна и дорог

        generateTerrain();
        seeder = new System.Random();
			
		voroObject = new Voronoi ( 1 );
        spreadPoints();
        
       Man.ChangePlaneSize(height, width);
       
    }
    void Start(){
        foreach (GameObject item in Roads)
        {
            // регестрация всего что было построено
            item.GetComponent<BuildConfig>().GenerateBuildMap();//можно попытаться без этого этапа, но это не сильно повердит
            item.GetComponent<BuildConfig>().Build();
            
        }
        Im.material.mainTexture = image;
        Roads.Clear();
        CityManager.GetCityNavMeshSurface().BuildNavMesh();
        CityManager.StartLoad();
    }
}
