﻿using UnityEngine;
using System.Collections;

public class getnoise : MonoBehaviour {
	public static float[,] GetSimplePerlin(float StartX,float StartY,float Width,float Height,float Scale,float PixRange, float power,float fadecoff){ 
		float[,] map = new float[(int)Width, (int)Height];

		for(int x = 0; x < (int)Width;x++){
			for(int y = 0;y < (int)Height;y++){
				float Xcoord = (StartX + (float)x) / Scale;
				float Ycoord = (StartY + (float)y) / Scale;
				map [x, y] = Mathf.PerlinNoise (Xcoord, Ycoord) * power; // задание точке x,y на карте шума с применением маски и мощности
			}
		}
		return map;
	} // тестовый простой вариант шума, возвращал карту высот простого шума (в конечной версии не используется)
	/*public static Texture2D GetTextureFromHeights(float[,] heights, int width,int height){
		Texture2D tex = new Texture2D(width,height);
		for(int i = 0;i < width;i++){
			for(int g = 0;g < height;g++){
				tex.SetPixel (i, g, new Color (heights [i, g], heights [i, g], heights [i, g]));
			}
		}
		tex.Apply ();
		return tex;
	}*/ // тестовая функция, переводила карту высот в текстуру (в конечной версии не используется)
	public static float[,] GetMaskedPerlin(float StartX,float StartY,float Width,float Height,float Scale,float PixRange, float power,float fadecoff){// возвращает шум с примененной маской
		// принимает длину, ширину, координаты начаа, размер шага, мощность, коэффициэнт яркости маски, радиус маски
		float[,] map = new float[(int)Width, (int)Height]; // создание мустой карты
		float[,] mask = CreateMask ((int)Width,(int)Height,PixRange,fadecoff); // получение маски 
		for(int x = 0; x < (int)Width;x++){ 
			for(int y = 0;y < (int)Height;y++){
				float Xcoord = (StartX + (float)x) / Scale;
				float Ycoord = (StartY + (float)y) / Scale;
				map [x, y] = Mathf.PerlinNoise (Xcoord, Ycoord) * mask[x,y] * power; // задание точке x,y на карте шума с применением маски и мощности
			}
		}
		return map; // возвращает карту высот
	}
	public static Vector3[] GetSimpleNoiseVector3(float StartX,float StartY,float Scale,Vector3[] vertices,float power){ 
		// вариант шума, адаптированный для мэшей, требует меньше действий из-за отсутствия перехода из float[,] в Vector3[]
		// не использует маску, требует передачи вершин
		for(int i = 0 ; i < vertices.Length;i++){
			float Xcoord = (StartX + vertices[i].x) / Scale;
			float Ycoord = (StartY + vertices[i].z) / Scale; // в изпользуемых мною мэшах 11 вершин, центр начинается с середины, тоесть вершины по x и z нумируются от 5;5 до -5;-5, между блоками следует учитывать
			// кол-во вершин от центров этих блоков, чтобы они "склеились" и небыли одинаковыми
			vertices [i].y = Mathf.PerlinNoise (Xcoord,Ycoord) * power; // запись в i-ю вершину по счету шума с учетом мощности
		}
		return vertices; // возвращает вершины
	}
	public static float[,] CreateMask(int Width,int Height, float PixRange, float fadecoff){ // создает маску для применения к карте высот, принимает длину, ширину, радиус маски и коэффициэнт яркости
		float [,] buff = new float[Width,Height]; // создание пустой маски
		float centerX = Width / 2f;
		float centerY = Height / 2f; // нахождение центра
		for(int x = 0; x < Width;x++){
			for(int y = 0; y < Height;y++){ 
				float Dist = Vector2.Distance (new Vector2(x,y),new Vector2(centerX,centerY));// нахождение расстояния рассматриваемой точки от цетра
				if(Dist <= PixRange){
					buff [x, y] = (1 - Dist / PixRange) * fadecoff; // применение соответствующей точке на маске значения от 0.0 до 1.0, которое зависит от расстояния от центра и радиуса маски.
					// позже умножается на коэффициэнт яркости
				}
			}
		}
		return buff;
	}
	public static float change(float s, float a1, float a2, float b1, float b2)
	{
		return b1 + (s-a1)*(b2-b1)/(a2-a1);
	}	
	public static float[,] GetMaskedMultiNoise(int Width, int Height, float scale, int octaves, float persistence, float lacunarity,float Power) {
		// создает карту высот на основе наложения измененных слоев шума перлина
		// принимает ширину, длину, шаг шума,кол-во октав(шумов), стойкость (детальность, каменистость) , неоднородность( площадь охвата),мощность, радиус маски, коэффициэнт яркости маски
		//float[,] Mask = CreateMask (Width, Height, PixRange,fadecoff); //получение маски
		float[,] noiseMap = new float[Width,Height]; // создание пустой карты 
		Vector2[] octaveOffsets = new Vector2[octaves]; // массив с начальными координатами разных слоев шума
		System.Random random = new System.Random ();
		for (int i = 0; i < octaves; i++) {
			float offsetX = random.Next(-100000,100000);
			float offsetY = random.Next(-100000,100000);
			octaveOffsets [i] = new Vector2 (offsetX, offsetY); // для каждого слоя заданы случайные координаты
		}
		float maxNoiseHeight = float.MinValue;
		float minNoiseHeight = float.MaxValue; // максимальная и минимальная высота, на данный момент не записана

		for (int y = 0; y < Height; y++) {
			for (int x = 0; x < Width; x++) {
				float noiseHeight = 0;
				float amplitude = 1; // амплитуда (высота)
				float frequency = 1; // частотность (детальность), можно изменять, однако то-же самое меняет persistance и lacunarity,  промежуточные значения, необходимые для работы алгоритма
				for (int i = 0; i < octaves; i++) { // получение всех карт шума
					float sampleX = x / scale * frequency + octaveOffsets[i].x;
					float sampleY = y / scale * frequency + octaveOffsets[i].y; // отсчет ведется от центра, как при создании воды и песка 

					float perlinValue = (Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1); // получение инвертированного шума перлина
					noiseHeight += perlinValue * amplitude; // прибавление высоты, умноженной на амплитуду к перменной высоты шума, необходима для получения наложенныго шума

					amplitude *= persistence; // изменение амплитуды на стойкость
					frequency *= lacunarity; // изменение частоты на масштаб (создание детальности, маленьких "скал")
				}

				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				} else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				} // поиск минимальное и максимаьное значение шума по мере получения этих значений
				noiseMap [x, y] = noiseHeight * Power ; // применение к точке x;y общего шума для всех слоев с приминением мощности
			}
		}
		for (int y = 0; y < Height; y++) {
			for (int x = 0; x < Width; x++) {
				noiseMap [x, y] = change (noiseMap [x, y], minNoiseHeight, maxNoiseHeight, 0f, 0.3f);  // применение Инверссированной линейной интерполяции (нахождение промекжуточного 
				//значения между двумя значениями на основе третьего) со значениями минимальной высоты, максимальной высоты, значения шума в этой точке, последующее применение маски
			}
		}
		return noiseMap; // возвращает карту высот
	}
}
