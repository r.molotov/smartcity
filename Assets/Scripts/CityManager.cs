﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UnityEngine.UI;
using UnityEngine.AI;

public class CityManager : MonoBehaviour
{
    #region Singleton
    private static CityManager _instance;
    public static CityManager Instance() { return _instance; }
    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }

    }
    #endregion Singleton

    [SerializeField] GameObject City;
    [SerializeField] GameObject PetyaPrefab;
    [SerializeField] GameObject Clock;
    public GameObject TestPlayer;
    public GameObject SpawnPoint;
    [SerializeField] int PeapleCount=200;
    GameObject[,] World;
    public GameObject[] Builds;
    [SerializeField] string[] BuildNames = new string[] { "дорога", "маленький дом", "дом культуры", "большой дом", "школа", "стадион" };
    NavMeshSurface CityNavMeshSurface;
    int[][,] BuildingConfigurations;
    [SerializeField] GameObject road2;
    NewPController[] AllPetya;
    int GlobalTime = 0;
    List<PointInfo> Points = new List<PointInfo>(0);
    bool Change = false;
    public GameObject CanvasSustem;
   


    // Start is called before the first frame update
    void Start()
    {
        
        CityNavMeshSurface = City.GetComponent<NavMeshSurface>();
        //CityNavMeshSurface.BuildNavMesh();
        //CityNavMeshSurface.UpdateNavMesh(CityNavMeshSurface.navMeshData);

        BuildingConfigurations = BuildConfig.Instance().GetConfig();
        


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            //книпка режима отладки для перезапуска дня

            GetAllPoints();
            GlobalTime = 0;
            Change = true;
            
        }

        if (Change)
        {
            //при каждом изменениии вермени сообщает каждому человек новое время. Позволяет 
            //синхронизировать время. Мнеяет часы на экране

            Clock.GetComponent<Text>().text = GlobalTime.ToString();
            foreach (NewPController man in AllPetya)
            {
                man.OnChangeTime(GlobalTime);
            }
            Change = false;
        }

    }

    void GetAllPoints()
    {
        //Берёт из потомков City тех, у которых тег IPoint и добавляет
        //их в массив Points, при этом длина Points равна колличеству
        Points.Clear();
        foreach (Transform building in City.transform)
        {
            if (building.gameObject.tag == "IPoint")
            {
                Points.Add(building.gameObject.GetComponent<PointInfo>());
                
            }
        }
        
    }

    void CreatePetya(int Count)
    {
        //Создаёт count людей, каждый появляется в случайно точке интереса
        //и добавляет в массив OllPeta при этом длина массива равна count
        AllPetya = new NewPController[Count];
        
        for (int i = 0; i < Count; i++)
        {
            Transform Home = null;
            for (int j = 0; j < 1000; j++)
            {
                int a = Random.Range(0, Points.ToArray().Length - 2);
                PointInfo tmp = Points[a];
                if (tmp.Liveble)
                {
                    Home = tmp.spawnpoint.transform;
                    break;
                }
            }

            GameObject PetyaClon = GameObject.Instantiate(PetyaPrefab,Home.position,Quaternion.identity, City.transform);
            
            
            
            
            AllPetya[i] = PetyaClon.GetComponent<NewPController>();
            AllPetya[i].AddHome(Home);
            AllPetya[i].transform.localScale = new Vector3(0.04f, 0.4f, 0.04f);

            AllPetya[i].GetComponent<MeshRenderer>().enabled = false;


        }

    }

    void TimerStart()
    {
        // метод создания таймиров. Я честно не знаю, как он работает. Просто код нашёл.
        // срабатывает и вызывает метод ChangeTime каджые 10000 милисекунд == 10 сек.
        TimerCallback TimerFunction = new TimerCallback(OnChangeTime);
        Timer MainTimer = new Timer(TimerFunction, null, 0, 10000);
    }

    void OnChangeTime(object TmpObject)
    {
        //метод выхывается через каждые 10 секунд и меняет вермя
        // реальзовать передачу данных людям отсюда незльзя

        GlobalTime++;
        if (GlobalTime == 25)
        {
            GlobalTime = 0;
        }
        Change = true;


    }

    void CheckRoad(int x, int z)
    {
        //данный метод позволяет ставить скошеные элементы дороги на мето тех, у которых только 2 соседа
        // под углом 90 градусов друг к другу. Данный метод не оптимален, но на данный момент выполняет поставленую задачу
        int[,] coordinates = { { x + 1, z }, { x - 1, z }, { x, z + 1 }, { x, z - 1 }, { x, z } };




        for (int i = 0; i < coordinates.GetLength(0); i++)
        {
            GameObject gtmp = World[coordinates[i, 0], coordinates[i, 1]];
            if (gtmp != null && gtmp.tag == "road")
            {


                int[] neighbors = new int[4];
                neighbors[0] = ((World[coordinates[i, 0] + 1, coordinates[i, 1]] != null) && (World[coordinates[i, 0] + 1, coordinates[i, 1]].tag == "road")) ? 1 : 0;
                neighbors[1] = ((World[coordinates[i, 0] - 1, coordinates[i, 1]] != null) && (World[coordinates[i, 0] - 1, coordinates[i, 1]].tag == "road")) ? 1 : 0;
                neighbors[2] = ((World[coordinates[i, 0], coordinates[i, 1] + 1] != null) && (World[coordinates[i, 0], coordinates[i, 1] + 1].tag == "road")) ? 1 : 0;
                neighbors[3] = ((World[coordinates[i, 0], coordinates[i, 1] - 1] != null) && (World[coordinates[i, 0], coordinates[i, 1] - 1].tag == "road")) ? 1 : 0;
                if (neighbors[0] == 1 && neighbors[1] == 0)
                {
                    if (neighbors[2] == 0 && neighbors[3] == 1)
                    {
                        Vector3 tmp = gtmp.transform.position;
                        tmp.y += 0.01f;
                        Destroy(gtmp);
                        GameObject g = GameObject.Instantiate(road2, tmp, Quaternion.identity, City.transform);
                        g.transform.Rotate(new Vector3(0, -90, 0));
                        World[coordinates[i, 0], coordinates[i, 1]] = g;

                    }
                    else if (neighbors[2] == 1 && neighbors[3] == 0)
                    {
                        Vector3 tmp = gtmp.transform.position;
                        Destroy(gtmp);
                        tmp.y += 0.01f;
                        GameObject g = GameObject.Instantiate(road2, tmp, Quaternion.identity, City.transform);
                        g.transform.Rotate(new Vector3(0, 180, 0));
                        World[coordinates[i, 0], coordinates[i, 1]] = g;

                    }
                    else if (gtmp.transform.childCount == 1)
                    {
                        Vector3 tmp = gtmp.transform.position;
                        Destroy(gtmp);
                        tmp.y -= 0.01f;
                        GameObject g = GameObject.Instantiate(Builds[0], tmp, Quaternion.identity, City.transform);

                        World[coordinates[i, 0], coordinates[i, 1]] = g;
                    }
                }
                else if (neighbors[0] == 0 && neighbors[1] == 1)
                {
                    if (neighbors[2] == 0 && neighbors[3] == 1)
                    {
                        Vector3 tmp = gtmp.transform.position;
                        tmp.y += 0.01f;
                        Destroy(gtmp);
                        GameObject g = GameObject.Instantiate(road2, tmp, Quaternion.identity, City.transform);
                        g.transform.Rotate(new Vector3(0, 0, 0));
                        World[coordinates[i, 0], coordinates[i, 1]] = g;

                    }
                    else if (neighbors[2] == 1 && neighbors[3] == 0)
                    {
                        Vector3 tmp = gtmp.transform.position;
                        tmp.y += 0.01f;
                        Destroy(gtmp);
                        GameObject g = GameObject.Instantiate(road2, tmp, Quaternion.identity, City.transform);
                        g.transform.Rotate(new Vector3(0, 90, 0));
                        World[coordinates[i, 0], coordinates[i, 1]] = g;

                    }
                    else if (gtmp.transform.childCount == 1)
                    {
                        Vector3 tmp = gtmp.transform.position;
                        tmp.y -= 0.01f;
                        Destroy(gtmp);
                        GameObject g = GameObject.Instantiate(Builds[0], tmp, Quaternion.identity, City.transform);

                        World[coordinates[i, 0], coordinates[i, 1]] = g;
                    }
                }
                else if (gtmp.transform.childCount == 1)
                {
                    Vector3 tmp = gtmp.transform.position;
                    Destroy(gtmp);
                    tmp.y -= 0.01f;

                    GameObject g = GameObject.Instantiate(Builds[0], tmp, Quaternion.identity, City.transform);

                    World[coordinates[i, 0], coordinates[i, 1]] = g;
                }


            }

        }


    }

    public GameObject CheckPlace(int PlaceID)
    {
        // проверяет, есть ли в городе нужное место
        //если да, то выводит его Transform
        foreach (PointInfo Point in Points)
        {

            if (Point.GetIntID() == PlaceID)
            {
                return Point.spawnpoint;
            }
        }
        return null;
    }

    public void AddNewBuild(GameObject build)
    {
        // добавляет здание в массив world
        int x = (int)build.transform.position.x;
        int z = (int)build.transform.position.z;
        World[x, z] = build;
        if (build.tag == "road")
        {
            CheckRoad(x, z);
        }
    }

    public void AddNewPoint(GameObject Point)
    {
        //метод необходим для добавления здания
        Points.Add(Point.GetComponent<PointInfo>());

        
    }

    public void RemovePoint(Transform point)
    {
        //метод необходим для удаления здания
        Points.Remove(point.GetComponent<PointInfo>());
        foreach (NewPController item in AllPetya)
        {
            //item.CheckRemovePlase(point.GetComponent<PointInfo>().spawnpoint.transform);
        }
    }

    public void ChangePlaneSize(int x, int y)
    {
        // метод подгоняет террейн и массив мира под размеры сгенерированного мира
        //City.transform.GetChild(0).GetComponent<Terrain>().terrainData.size = new Vector3(x, 600, y);

        World = new GameObject[x, y];
    }

    public bool CheckFreeSpace(int x, int y)
    {
        // проверка, можно ли строить на этом месте, не перекрывет ли постройка 
        //другую, построеную ранее
        
         return (World[x, y] == null);
       
    }

    public void RotateMatrix(int Type)
    {
        // метод для поаворота матрицы на 90 градусов(код позаимствован из сети)
        int n = BuildingConfigurations[Type].GetLength(0);
        int[,] tmpres = new int[n, n];

        for (int i = 0; i <= n - 1; ++i)
        {
            for (int j = 0, t = n - 1; j <= n - 1; ++j, --t)
            {
                tmpres[i, j] = BuildingConfigurations[Type][t, i];

            }
        }
        BuildingConfigurations[Type] = tmpres;
    }

    public void NewTargetToAll(Vector3 v3)
    {
        foreach (NewPController item in AllPetya)
        {
            item.DEBUG_GoTo(v3);

        }
    }

    //Статичный методы-------------------------------------------------------
    public static int GetMaxID()
    {
        return CityManager.Instance().BuildNames.Length - 1;
    }

    public static string GetNameFromID(int ID)
    {
        //вспомогательный метод вывода имени по ID

        return Instance().BuildNames[ID];
    }

    public static GameObject GetRandomPlases()
    {
        // Статичный метод получения рандомной точки В дальнейшем будет
        // заменём на более сложный алгоритм цели
        return Instance().Points[Random.RandomRange(0, Instance().Points.ToArray().Length - 1)].spawnpoint;

    }

    public static GameObject GetCity()
    {
        // возвращает обект города
        return CityManager.Instance().City;
    }

    public static NavMeshSurface GetCityNavMeshSurface()
    {
        // возвращает компонент для обновления навигатора
        return CityManager.Instance().CityNavMeshSurface;
    }

    public static int[,] GetNowBuildingConfiguration(int type)
    {
        // возвращает текужую конфигурация здания
        return CityManager.Instance().BuildingConfigurations[type];
    }

    public static string[] GetAllBuildNames()
    {
        //возвращает имена всех зданий
        return CityManager.Instance().BuildNames;
    }

    public static void StartLoad()
    {
        CityManager.Instance().GetAllPoints();
        CityManager.Instance().CreatePetya(CityManager.Instance().PeapleCount);
        
        CityManager.Instance().TimerStart();
    }



    public void TEST_CreateTestPalyer(Vector3 v3)
    {
       
    TestPlayer = GameObject.Instantiate(PetyaPrefab, v3, Quaternion.identity, null);
    }
    
  


}
