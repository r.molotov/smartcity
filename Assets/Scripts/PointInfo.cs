﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointInfo : MonoBehaviour
{ 

    public int MyID = -1 ;
    public GameObject spawnpoint;
    Dictionary<string,int> Organization = new Dictionary<string, int> {
        { "дорога",0 },
        { "маленький дом",1 },
        { "дом культуры",2 },
        { "большой дом",3 },
        { "школа", 4 },
        { "стадион",5 }
};
    GameObject[] MyBuild;
    string[] Names = new string[] { "дорога", "маленький дом", "дом культуры", "большой дом", "школа", "стадион" };
    int[] LivePlase = { 1, 3 };
    public bool Liveble = true;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public string GetMyID()
    {
        //вспомогательный метод для ввода имени этого здания
        return Names[MyID];
    }

    public int GetIntID()
    {
        // вспомогательный метод по получению ID
        return MyID;
    }

    public void СhangeID(string t1)
    {
        //Меняет ID мета по имени. на данный момент не используется
            MyID = Organization[t1]-1;
    }

    public void InstalBuild(GameObject[] tmp,int id,GameObject Spawnpoint)
    {
        //добавления оснавных параметров в здания. вызывается при его оздании
        MyBuild = tmp;
        MyID = id;
        spawnpoint = Spawnpoint;
        
    }
    
    public GameObject[] GetAllBuild()
    {
        //метод возвращает все части одного здания
        return MyBuild;
    }

    
}
