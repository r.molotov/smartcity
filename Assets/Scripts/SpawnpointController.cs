﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnpointController : MonoBehaviour
{
    [HideInInspector] public PointInfo MyPoint;

    // Start is called before the first frame update

    public int Check()
    {
        //сюда запишется инфо о пересечении луча, если оно будет
        RaycastHit hit;
        //сам луч, начинается от позиции этого объекта и направлен в сторону цели
        Ray ray = new Ray(transform.position, new Vector3(0, -10f, 0));
        //пускаем луч
        if (Physics.Raycast(ray, out hit)) {
            if (hit.collider != null && hit.collider.gameObject.GetComponent<Terrain>())
            {
                
                transform.position= hit.point;
                return 0;

            }
        }
        ray = new Ray(transform.position, new Vector3(0, 10f, 0));
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.GetComponent<Terrain>())
            {

                transform.position = hit.point;
                return 0;
            }
        }
        return 1;
    }
}
